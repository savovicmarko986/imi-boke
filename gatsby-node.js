const path = require("path");

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const projectTemplate = path.resolve(
    "src/templates/project-template/Project.js"
  );
  const newsTemplate = path.resolve("src/templates/news-template/News.js");
  const projects = await graphql(`
    query {
      allContentfulProject(filter: { isNews: { eq: false } }) {
        edges {
          node {
            slug
            id
          }
        }
      }
    }
  `);
  const news = await graphql(`
    query {
      allContentfulProject(filter: { isNews: { eq: true } }) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);
  const allNews = await graphql(`
    query {
      allContentfulProject {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);
  projects.data.allContentfulProject.edges.forEach((edge) => {
    createPage({
      component: projectTemplate,
      path: `/projects/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
        id: edge.node.id,
      },
    });
  });
  news.data.allContentfulProject.edges.forEach((edge) => {
    createPage({
      component: newsTemplate,
      path: `/news/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
        isAll: false,
      },
    });
  });
  allNews.data.allContentfulProject.edges.forEach((edge) => {
    createPage({
      component: newsTemplate,
      path: `/all-news/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
        isAll: true,
      },
    });
  });
  allNews.data.allContentfulProject.edges.forEach((edge) => {
    createPage({
      component: newsTemplate,
      path: `/single-project/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
        isSingleProject: true,
      },
    });
  });
};
