import React from "react";
import Avatar from "../svgs/Avatar/Avatar";
import { format } from "date-fns";

export default function Comment({ username, comment, published }) {
  const dateToShow = published
    ? format(new Date(published * 1000), "dd/MM/yyyy hh:mm")
    : null;
  return (
    <div key={`${comment}-${published}`} role="listitem">
      <p style={{ display: "flex", alignItems: "center", color: "black" }}>
        <Avatar /> <b>{username}</b>
        <span
          style={{
            display: "inline-block",
            marginLeft: "auto",
            fontSize: "0.8rem",
          }}
        >
          {dateToShow ? dateToShow : ""}
        </span>
      </p>
      <p style={{ color: "black", paddingLeft: "24px" }}>{comment}</p>
    </div>
  );
}
