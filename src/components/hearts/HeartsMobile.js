import React from "react";
import {Link, navigate} from "gatsby";
import useLang from "../../contexts/Language/LanguageContext";
import translate from "../../utils/lang/langHash";
import FavoriteIcon from '@material-ui/icons/Favorite';
import { container, wrapper, mobContainer, badgeContainer} from "./Hearts.module.scss";
import Badge from '@material-ui/core/Badge';

export default function HeartsMobile() {
    const lang = useLang();
    return (
        <div className={mobContainer}>
            <div className={badgeContainer} role="navigation" onClick={() => navigate("/donate")}>
                <Badge color="primary" anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }} badgeContent={translate("donate", lang)}
                  >
                    <FavoriteIcon 
                    style={{width: "40px", height: "40px", color: "rgba(240, 91, 136, 1)", cursor: "pointer"}} 
                    />
                </Badge>
            </div>
            <div className={badgeContainer} role="navigation" onClick={() => navigate("/membership")}>
                <Badge color="primary" anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }} badgeContent={translate("membership", lang)}
                >
                    <FavoriteIcon 
                    style={{width: "40px", height: "40px", color: "#00adee", cursor: "pointer"}} 
                    />
                </Badge>
            </div>
        </div>
    )

}