import * as React     from "react";
import { useSpring, useTransition, animated, config } from "react-spring";



const newText = (text) => ({ key : `${Date.now()}`, data : text });

const TextTransition = ({
	text,
	inline,
	delay,
	className,
	style,
	noOverflow,
	springConfig
}) => {
	const placeholderRef              = React.useRef(null);
	const [content, setContent]       = React.useState(() => newText(text.toString()));
	const [timeoutId, setTimeoutId]   = React.useState(0);
	const [isFirstRun, setIsFirstRun] = React.useState(true);
	const [width, setWidth]           = React.useState({ width : inline ? 0 : "auto" });
	const transitions                 = useTransition(content, (item) => item.key, {
		from        : { opacity : 0 },
		enter       : { opacity : 1 },
		leave       : { opacity : 0 },
		config      : springConfig,
		immediate   : isFirstRun,
		onDestroyed : () => {
			setIsFirstRun(false);
		}
	});
	const animatedProps = useSpring({
		to        : width,
		config    : springConfig,
		immediate : isFirstRun,
	});
	React.useEffect(() => {
		setTimeoutId(
			setTimeout(() => {
				if (!placeholderRef.current) return;
				placeholderRef.current.innerHTML = text.toString();
				if (inline) setWidth({ width : placeholderRef.current.offsetWidth });
				setContent(newText(text.toString()));
			}, delay)
		);
	}, [text]);

	React.useEffect(() => () => clearTimeout(timeoutId), []);

	return (
		<animated.div
			className={ `text-transition ${className}` }
			style={{
				...animatedProps,
				whiteSpace : inline ? "nowrap" : "normal",
				display    : inline ? "inline-block" : "block",
				position   : "relative",
				...style,
			}}
		>
			<span ref={ placeholderRef } style={{ visibility : "hidden" }} className="text-transition_placeholder" />
			<div
				className="text-transition_inner"
				style={{
					overflow : noOverflow ? "hidden" : "visible",
					display  : "block",
					position : "absolute",
					top      : 0,
					left     : 0,
					height   : "100%",
					width    : "100%"
				}}
			>
				{
					transitions.map(({ item, props, key }) => (
						<animated.div key={ key } style={ { ...props, position : "absolute" } }>{ item.data }</animated.div>
					))
				}
			</div>
		</animated.div>
	);
};



export default TextTransition;
export { config as presets };