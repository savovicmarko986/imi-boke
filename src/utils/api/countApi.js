import axios from "axios";

export async function incrementViewCount(page) {
  const res = await axios.get(`https://api.countapi.xyz/hit/imiboke/${page}`);
  return res.data.value;
}
