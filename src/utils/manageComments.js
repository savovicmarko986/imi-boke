import db from "./firebase";
import { collection, query, where, getDocs, addDoc } from "firebase/firestore";

const getCommentsBySlug = async (slug) => {
  const q = query(collection(db, "comments"), where("slug", "==", slug));
  const querySnapshot = await getDocs(q);
  const data = [];
  querySnapshot.forEach((doc) => {
    // doc.data() is never undefined for query doc snapshots
    data.push(doc.data());
  });
  return data;
};

const postComment = async ({ comment, username, slug }) => {
  const collectionRef = collection(db, "comments");
  const payload = {
    value: comment,
    username,
    published: false,
    time: new Date(),
    slug,
  };
  const docRef = await addDoc(collectionRef, payload);
  return docRef;
};

export { getCommentsBySlug, postComment };
