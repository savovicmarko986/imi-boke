import useMediaQuery from '@material-ui/core/useMediaQuery';

const useIsMobile = () => {
    const isMobile = useMediaQuery('(max-width:700px)');
    return isMobile
}

export default useIsMobile;