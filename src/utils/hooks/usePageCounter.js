import { useEffect, useState } from "react";
import { incrementViewCount } from "../api/countApi";

export function usePageCounter(pageName) {
  const [count, setCount] = useState();
  useEffect(() => {
    incrementViewCount(pageName)
      .then((val) => setCount(val))
      .catch((err) => console.error(err));
  }, []);
  return count;
}
