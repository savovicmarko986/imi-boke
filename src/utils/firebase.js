import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const config = {
  apiKey: "AIzaSyDmVm_QUR_Z4hAt1dMWm9-f1iX6IUXl3hs",
  authDomain: "imi-boke.firebaseapp.com",
  projectId: "imi-boke",
  storageBucket: "imi-boke.appspot.com",
  messagingSenderId: "495881234860",
  appId: "1:495881234860:web:acd6712290b4db9d67f209",
};

const app = initializeApp(config);

// const firestore = firebase.firestore();

// export { firestore };

export default getFirestore(app);
